import './App.css';
import Task from './modules/Task'
import avatar from './content/3.jpg'
import React from 'react'
import { useState} from 'react';

function App() {
  let [inputValue, setInputValue] = useState('')
  let [tasks, setTasks] = useState([])

  const addTask = ()=>{
    if(inputValue!==''){
      setInputValue('')
      setTasks(
        tasks = [...tasks, {id:tasks.length, textValue: inputValue}]
      )
      console.log(tasks)
    }
    else{
      alert('Pole is empty')
    }

  }
  const removeTask = index =>{
    
    setTasks(
      tasks = tasks.filter(task => task.id !== index).map((task,id)=>{
        return {...task, id: id}
      })
    )
    console.log(tasks)
  }

  return (
    <div className="App">
      <div className="contentWrapper">
        <div className="mainWrapper">
            <div className="header">
                <div className="header__imageWrapper">
                    <img src={avatar} className="header__avatar" alt=''/>
                </div>
            </div>
            <div className="content">
                <div className="content__tasksWrapper">
                    <h1>My Tasks</h1>
                    <div className="content__tasksContainer">
                        {tasks.map(task=>{
                            return <Task removeTask={()=>{removeTask(task.id)}} textValue={task.textValue} key={task.id}/>
                          })}
                    </div>
                </div>            
                <div className="content__buttons">
                    <input type="text" className="content__input" onInput={e=>{setInputValue(inputValue = e.target.value)}} value={inputValue}/>
                    <button className="content__addTaskButton" onClick={addTask}>Add task</button>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}

export default App;
