import React from 'react'
import { useState } from 'react'

export default function Task(props){
  let [taskStatus, setTaskStatus] = useState('content__task')

  const disableTask = ()=>{
    if(taskStatus === 'content__task'){
      setTaskStatus(
        taskStatus = 'content__task content_task-disable'
      )
    }
    else{
      setTaskStatus(
        taskStatus = 'content__task'
      )
    }
  }

  
  
  return(
    <div className="content__taskWrapper">
      <div className="content__taskMain">
        <input type="checkbox" onChange={disableTask}/>
        <p className={taskStatus}>{props.textValue}</p>
      </div>
      <button className='btn btn-primary' onClick={props.removeTask}>Remove task</button>
    </div>
  )
}